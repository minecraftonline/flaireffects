package com.minecraftonline;

import org.slf4j.Logger;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Plugin;

import com.google.inject.Inject;

@Plugin(
        id = "flaireffects",
        name = "FlairEffects",
        authors = { "Anna_28" },
        description = "A plugin that creates particle and sound effects upon teleports, movement, or other events."
)
public class FlairEffects
{
    @Inject
    private Logger logger;

    @Listener
    public void onServerStart(GameStartedServerEvent event)
    {
        logger.info("FlairEffects Started");
    }
}